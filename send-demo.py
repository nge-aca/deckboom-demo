"""
send-demo.py <recv-queue> [<reply-queue>]
"""
import boto3
import sys

BODY = "<HostQueryRq/>"

if len(sys.argv) >= 3:
    recv = sys.argv[1]
    reply = sys.argv[2]
else:
    recv = sys.argv[1]
    reply = None

sqs = boto3.resource('sqs')
queue = sqs.Queue(recv)

mattrs = {}

if reply:
    mattrs['replyQueue'] = {
        'DataType': 'String.URL',
        'StringValue': reply,
    }

print(queue.send_message(MessageBody=BODY, MessageAttributes=mattrs))
